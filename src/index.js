import React from 'react';
import ReactDOM from 'react-dom';
import { Route } from 'react-router'
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import store, { history } from './store'
import './index.css';
import Header from './components/Header';
import Home from './scenes/Home';
import Login from './scenes/Login';
import EditPlan from './scenes/Plan/edit';
import Country from './scenes/Counrty';
import EditCountry from './scenes/Counrty/edit';
import City from './scenes/City';
import EditCity from './scenes/City/edit';
import Currency from './scenes/Currency';
import EditCurrency from './scenes/Currency/edit';
import User from './scenes/User';
import EditUser from './scenes/User/edit';
import Quotation from './scenes/Offer';

import registerServiceWorker from './registerServiceWorker';
import PrivateRoute from './components/PrivateRoute';

let graphQlURL = 'http://localhost:4000/gql';

if (process.env.NODE_ENV === 'production') {
    graphQlURL = 'http://losmejoresviajes.co:4000/gql';
}

const client = new ApolloClient({
    link: new HttpLink({ uri: graphQlURL }),
    cache: new InMemoryCache({
        addTypename: false
    })
});

ReactDOM.render(
    <ApolloProvider client={client}>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <div>
                    <Header />
                    <main>
                        <PrivateRoute exact path="/admin" component={Home} />
                        <PrivateRoute exact path="/admin/plan/:id" component={EditPlan} />
                        <PrivateRoute exact path="/admin/country" component={Country} />
                        <PrivateRoute exact path="/admin/country/:id" component={EditCountry} />
                        <PrivateRoute exact path="/admin/city" component={City} />
                        <PrivateRoute exact path="/admin/city/:id" component={EditCity} />
                        <PrivateRoute exact path="/admin/currency" component={Currency} />
                        <PrivateRoute exact path="/admin/currency/:id" component={EditCurrency} />
                        <PrivateRoute exact path="/admin/user" component={User} />
                        <PrivateRoute exact path="/admin/user/:id" component={EditUser} />
                        <PrivateRoute exact path="/admin/offer" component={Quotation} />
                        <Route path="/login" component={Login} />
                    </main>
                </div>
            </ConnectedRouter>
        </Provider>
    </ApolloProvider>,
    document.getElementById('root')
);
registerServiceWorker();
