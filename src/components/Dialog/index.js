import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import { withRouter } from 'react-router'

class AlertDialog extends React.Component {
    handleRequestClose = () => {
        if(this.props.redirectUrl) {
            this.props.history.push(this.props.redirectUrl);
        } else {
            window.location.reload();
        }
    };

    render() {
        return (
            <Dialog open={this.props.open} onRequestClose={this.handleRequestClose}>
                <DialogTitle>{this.props.message}</DialogTitle>
                <DialogContent>
                    <DialogContentText />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleRequestClose} color={this.props.error ? 'accent' : 'primary'} autoFocus>
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default (withRouter)(AlertDialog);
