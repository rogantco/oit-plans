import React, { Component } from 'react';
import Grid from 'material-ui/Grid';
import { InputLabel } from 'material-ui/Input';
import TextField from './Text';
import Autocomplete from './Autocomplete';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import Send from 'material-ui-icons/Send';
import { withStyles } from 'material-ui/styles';
import DeleteIcon from 'material-ui-icons/Delete';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

class Form extends Component {
    returnFieldComponent(field) {
        let component = '';
        if(field.type === 'autoComplete') {
            component = <Autocomplete field={field} />;
        } else {
            component = <TextField field={field} />;
        }

        return component
    }

    renderField(fields) {
        return fields.map((field, index) => {
            if(field.type === 'multiField'){
                const subFields = field.fields.map((fields, index) => {
                    fields[fields.length-1].size -= 1;
                    const subFieldsRow = this.renderField(fields);
                    if(typeof field.removeRow === 'function'){
                        subFieldsRow.push(<Grid item xs={1} key={fields.length}>
                            <Button color="accent" aria-label="remove row" onClick={() => field.removeRow(index)} style={{marginTop: '12px'}}>
                                <DeleteIcon />
                            </Button>
                        </Grid>)
                    }

                    return subFieldsRow;
                });

                const title = <Grid item xs={12} key={-1}><InputLabel>{field.label}</InputLabel></Grid>;
                subFields.unshift(title);

                if(typeof field.addMore === 'function'){
                    const addTbn = <Grid item xs={12} key={subFields.length} style={{textAlign: 'right'}}>
                        <Button fab color="primary" aria-label="add" onClick={field.addMore}>
                            <AddIcon />
                        </Button>
                    </Grid>;
                    subFields.push(addTbn);
                }

                return subFields
            }

            return(
                <Grid item xs={field.size} key={index}>
                    {this.returnFieldComponent(field)}
                </Grid>
            )
        })
    }

    render () {
        const { classes } = this.props;

        return (
            // eslint-disable-next-line
            <form action='javascript:;' onSubmit={this.props.onSubmit}>
                <Grid container spacing={40}>
                    {this.renderField(this.props.fileds)}
                    <Grid item xs={12} style={{textAlign: 'right'}}>
                        <Button type='submit' className={classes.button} raised color="primary">
                            Guardar
                            <Send className={this.props.classes.rightIcon} />
                        </Button>
                    </Grid>
                </Grid>
            </form>
        )
    }
}

export default withStyles(styles)(Form);
