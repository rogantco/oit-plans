import React from 'react';
import { FormControl } from 'material-ui/Form';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';

const TextField = (props) => (
    <FormControl fullWidth>
        <InputLabel>{props.field.label}</InputLabel>
        <Input
            {...props.field.model}
            type={props.field.type === 'currency' ? 'number' : props.field.type}
            startAdornment={props.field.type === 'currency' ? <InputAdornment position="start">$</InputAdornment> : ''}
            multiline={props.field.type === 'textarea'}
        />
    </FormControl>
);

export default TextField;
