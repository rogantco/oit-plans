import React from 'react';
import { Route, Redirect } from 'react-router';
import { connect } from 'react-redux';

class PrivateRoute extends Route {
    render() {
        let component = super.render();

        if (this.props.session.token) {
            return component;
        } else {
            return <Redirect to='/login' />;
        }
    }
}

const mapStateToProps = (state) => ({
    session: state.session,
    routing: state.routing
});

export default connect(
    mapStateToProps
)(PrivateRoute);
