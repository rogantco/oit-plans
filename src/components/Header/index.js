import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import AccountCircle from 'material-ui-icons/AccountCircle';
import Menu, { MenuItem } from 'material-ui/Menu';
import { Link } from 'react-router-dom';

const styles = theme => ({
    root: {
        marginBottom: theme.spacing.unit * 3,
        width: '100%'
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
});

class Header extends React.Component {
    state = {
        auth: true,
        anchorEl: null,
        openMenuNavigation: false,
        openMenuUser: false,
    };

    handleMenuNavigation = (event) => {
        this.setState({ anchorEl: event.currentTarget });
        this.setState({ openMenuNavigation: true });
    };

    handleMenuUser = (event, type) => {
        this.setState({ anchorEl: event.currentTarget });
        this.setState({ openMenuUser: true });
    };

    handleRequestClose = () => {
        this.setState({ anchorEl: null });
        this.setState({ openMenuNavigation: false });
        this.setState({ openMenuUser: false });
    };

    render() {
        const { classes } = this.props;
        const { auth, anchorEl } = this.state;

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="contrast" aria-label="Menu"
                            onClick={this.handleMenuNavigation}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            anchorEl={anchorEl}
                            open={this.state.openMenuNavigation}
                            onRequestClose={this.handleRequestClose}
                        >
                            <MenuItem onClick={this.handleRequestClose}><Link to={'/admin/user'}>Gestionar Usuarios</Link></MenuItem>
                            <MenuItem onClick={this.handleRequestClose}><Link to={'/admin'}>Gestionar Planes</Link></MenuItem>
                            <MenuItem onClick={this.handleRequestClose}><Link to={'/admin/country'}>Gestionar Países</Link></MenuItem>
                            <MenuItem onClick={this.handleRequestClose}><Link to={'/admin/city'}>Gestionar Ciudades</Link></MenuItem>
                            <MenuItem onClick={this.handleRequestClose}><Link to={'/admin/currency'}>Gestionar Monedas</Link></MenuItem>
                            <MenuItem onClick={this.handleRequestClose}><Link to={'/admin/offer'}>Consultar Cotizaciónes</Link></MenuItem>
                        </Menu>
                        <Typography type="title" color="inherit" className={classes.flex}>
                            Oit - Cotizador
                        </Typography>
                        {auth && (
                            <div>
                                <IconButton
                                    aria-owns={this.state.openMenuUser ? 'menu-appbar' : null}
                                    aria-haspopup="true"
                                    onClick={this.handleMenuUser}
                                    color="contrast"
                                >
                                    <AccountCircle />
                                </IconButton>
                                <Menu
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={this.state.openMenuUser}
                                    onRequestClose={this.handleRequestClose}
                                >
                                    <MenuItem onClick={this.handleRequestClose}>Profile</MenuItem>
                                    <MenuItem onClick={this.handleRequestClose}>My account</MenuItem>
                                </Menu>
                            </div>
                        )}
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);
