import React from 'react';
import classNames from 'classnames';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import FilterListIcon from 'material-ui-icons/FilterList';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';

const toolbarStyles = theme => ({
    root: {
        paddingRight: 2,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.A700,
                backgroundColor: theme.palette.secondary.A100,
            }
            : {
                color: theme.palette.secondary.A100,
                backgroundColor: theme.palette.secondary.A700,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
});

class EnhancedTableToolbar extends React.Component {
    state = {
        deleteDialog: false
    };

    handleDeleteDialog = () => {
        this.setState({ deleteDialog: true });
    };

    handleDeleteCancel = () => {
        this.setState({ deleteDialog: false });
    };

    handleDeleteConfirmed = () => {
        this.setState({ deleteDialog: false });
        this.props.handleDelete(this.props.selected);
    };

    render() {
        const { selected, classes } = this.props;

        return (
            <div>
                <Dialog open={this.state.deleteDialog} onRequestClose={this.handleDeleteCancel}>
                    <DialogTitle>{"Alerta"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Desea elmininar {selected.length} elemento(s)
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDeleteCancel} color="primary" autoFocus>
                            Cancelar
                        </Button>
                        <Button onClick={this.handleDeleteConfirmed} color="accent">
                            Eliminar
                        </Button>
                    </DialogActions>
                </Dialog>
                <Toolbar
                    className={classNames(classes.root, {
                        [classes.highlight]: selected.length > 0,
                    })}
                >
                    <div className={classes.title}>
                        {selected.length > 0 ? (
                            <Typography type="subheading">{selected.length} Seleccionado(s)</Typography>
                        ) : (
                            <Typography type="title">{this.props.title}</Typography>
                        )}
                    </div>
                    <div className={classes.spacer} />
                    <div className={classes.actions}>
                        {selected.length > 0 ? (
                            <Tooltip title="Eliminar"
                                     onClick={this.handleDeleteDialog}
                            >
                                <IconButton aria-label="Eliminar">
                                    <DeleteIcon />
                                </IconButton>
                            </Tooltip>
                        ) : (
                            <Tooltip title="Filter list">
                                <IconButton aria-label="Filter list">
                                    <FilterListIcon />
                                </IconButton>
                            </Tooltip>
                        )}
                    </div>
                </Toolbar>
            </div>
        );
    }
}

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    selected: PropTypes.array.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

export default EnhancedTableToolbar;
