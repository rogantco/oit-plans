import React, { Component } from 'react';
import ListTable from './../../components/ListTable';
import { graphql, withApollo, compose } from 'react-apollo';
import gql from 'graphql-tag';
import Button from 'material-ui/Button';
import { withRouter } from 'react-router'
import AlertDialog from './../../components/Dialog';

const columnData = [
    { id: 'name', numeric: false, disablePadding: false, label: 'Nombre' },
    { id: 'code', numeric: false, disablePadding: false, label: 'ISO' },
    { id: 'symbol', numeric: false, disablePadding: false, label: 'Simbolo' },
    { id: 'createdAt', numeric: false, disablePadding: false, label: 'Creado' },
    { id: 'updatedAt', numeric: false, disablePadding: false, label: 'Modificado' }
];

const curencyList = gql`query {
    currency {
        _id
        name
        code
        symbol
        createdAt
        updatedAt
    }
}`;


class Currency extends Component {
    state = {
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    createCurrency = () => {
        this.props.history.push('/admin/currency/new');
    };

    deleteCurrency = (selected) => {
        this.props.client.mutate({
            mutation: gql`mutation ($_ids: [ID!]) {
                currencyDelete (_ids: $_ids) {
                    _id
                }
            }`,
            variables: {
                _ids: selected
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Moneda(s) eliminada(s) correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div className="App">
                    <Button type='submit' raised color="primary" onClick={this.createCurrency}>
                        Nueva Moneda
                    </Button>
                    <ListTable columns={columnData} rows={data.currency} title={'Monedas'} url={'/admin/currency/'} handleDelete={this.deleteCurrency} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    withRouter,
    graphql(curencyList)
)(Currency);
