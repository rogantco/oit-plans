import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Form from './../../components/Form';
import {get, setWith, clone} from 'lodash';
import AlertDialog from './../../components/Dialog';

const currency = gql`query ($id: String) {
    currency (_id: $id) {
        _id
        name
        code
        symbol
    }
}`;

function bindModel(context) {
    return function model(path) {
        const value = get(context.state, path, '');

        return {
            value,
            onChange(event) {
                const newValue = typeof event === 'object' ? event.target.value : event;
                const newState = setWith(clone(context.state), path, newValue, clone);
                context.setState(newState);
            }
        };
    };
}

class EditCurrency extends Component {
    state = {
        currency: {},
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    componentWillReceiveProps (nextProps) {
        if(this.props.match.params.id === 'new') {
            this.setState({currency: {
                name: '',
                code: '',
                symbol: ''
            }});
        } else {
            this.setState({currency: nextProps.data.currency[0]});
        }
    };

    createFieldObject (model) {
        return [
            { label: 'Nombre', model: model('currency.name'), size: 12, type: 'text' },
            { label: 'Codigo ISO', model: model('currency.code'), size: 12, type: 'text' },
            { label: 'Simbolo', model: model('currency.symbol'), size: 12, type: 'text' }
        ]
    };

    mutate = () => {
        const currency = clone(this.state.currency);
        delete currency._id;
        this.props.client.mutate({
            mutation: gql`mutation ($_id: ID, $input: Currency_Input) {
                currency (
                    _id: $_id,
                    input: $input
                ) {
                    updatedAt
                }
            }`,
            variables: {
                _id: this.state.currency._id,
                input: currency
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Moneda moficada correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const model = bindModel(this);
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div>
                    <Form fileds={this.createFieldObject(model)} onSubmit={this.mutate} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                        redirectUrl={'/admin/currency'}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    graphql(currency, {
        options: (props) => ({
            variables: {id: props.match.params.id === 'new' ? '' : props.match.params.id}
        })
    })
)(EditCurrency);
