import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import IconButton from 'material-ui/IconButton';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Visibility from 'material-ui-icons/Visibility';
import VisibilityOff from 'material-ui-icons/VisibilityOff';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
} from 'material-ui/Dialog';

import { connect } from 'react-redux';
import { compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import { loggingUser } from './../../services/session/actions';

class Login extends Component {
    state = {
        email: '',
        password: '',
        showPassword: false,
    };

    logging = () =>  {
        this.props.client.query({
            query: gql`query ($email: String, $password: String) {
                session (email: $email, password: $password) {
                    _id,
                    name,
                    email,
                    token
                }
            }`,
            variables: {
                email: this.state.email,
                password: this.state.password
            }
        }).then((data) => {
            this.props.loggingUser(data.data.session);
        }).catch((err) => {
            console.log('catch', err)
        });
    };

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    };

    handleClickShowPasssword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };

    render() {
        return (
            <div className="Login">
                <Dialog open={true} onRequestClose={this.handleRequestClose}>
                    <DialogTitle>Login</DialogTitle>
                    <DialogContent>
                        <FormControl fullWidth>
                            <InputLabel htmlFor="email">Email Address</InputLabel>
                            <Input
                                id="email"
                                type="email"
                                value={this.state.email}
                                onChange={this.handleChange('email')}
                            />
                        </FormControl>
                        <FormControl fullWidth>
                            <InputLabel htmlFor="password">Password</InputLabel>
                            <Input
                                id="password"
                                type={this.state.showPassword ? 'text' : 'password'}
                                value={this.state.password}
                                onChange={this.handleChange('password')}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            onClick={this.handleClickShowPasssword}
                                        >
                                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.logging}
                            color="primary"
                        >
                            Continue
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    session: state.session
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    loggingUser,
}, dispatch);

export default compose(
    withApollo,
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(Login);
