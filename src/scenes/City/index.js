import React, { Component } from 'react';
import ListTable from './../../components/ListTable';
import { graphql, withApollo, compose } from 'react-apollo';
import gql from 'graphql-tag';
import Button from 'material-ui/Button';
import { withRouter } from 'react-router'
import AlertDialog from './../../components/Dialog';

const columnData = [
    { id: 'name', numeric: false, disablePadding: false, label: 'Nombre' },
    { id: 'country.name', numeric: false, disablePadding: false, label: 'País' },
    { id: 'createdAt', numeric: false, disablePadding: false, label: 'Creado' },
    { id: 'updatedAt', numeric: false, disablePadding: false, label: 'Modificado' }
];

const plansList = gql`query {
    city {
        _id
        name,
        country {
            _id
            name
        },
        createdAt,
        updatedAt
    }
}`;


class City extends Component {
    state = {
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    createCity = () => {
        this.props.history.push('/admin/city/new');
    };

    deleteCity = (selected) => {
        this.props.client.mutate({
            mutation: gql`mutation ($_ids: [ID!]) {
                cityDelete (_ids: $_ids) {
                    _id
                }
            }`,
            variables: {
                _ids: selected
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Moneda(s) eliminada(s) correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div className="App">
                    <Button type='submit' raised color="primary" onClick={this.createCity}>
                        Nueva Ciudad
                    </Button>
                    <ListTable columns={columnData} rows={data.city} title={'Ciudades'} url={'/admin/city/'} handleDelete={this.deleteCity} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    withRouter,
    graphql(plansList)
)(City);
