import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Form from './../../components/Form';
import {get, setWith, clone} from 'lodash';
import AlertDialog from './../../components/Dialog';

const city = gql`query ($id: String) {
    city (_id: $id) {
        _id
        name,
        country {
            _id
        }
    },
    country {
        _id
        name
    }
}`;

function bindModel(context) {
    return function model(path) {
        const value = get(context.state, path, '');

        return {
            value,
            onChange(event) {
                const newValue = typeof event === 'object' ? event.target.value : event;
                const newState = setWith(clone(context.state), path, newValue, clone);
                context.setState(newState);
            }
        };
    };
}

class EditCity extends Component {
    state = {
        city: {},
        country: [],
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    componentWillReceiveProps (nextProps) {
        this.setState({country: nextProps.data.country});

        if(this.props.match.params.id === 'new') {
            this.setState({city: {
                name: '',
                country: { _id: '' }
            }});
        } else {
            this.setState({city: nextProps.data.city[0]});
        }
    };

    createFieldObject (model) {
        return [
            { label: 'Nombre', model: model('city.name'), size: 12, type: 'text' },
            { label: 'País', model: model('city.country._id'), size: 12, type: 'autoComplete', suggestions: this.state.country }
        ]
    };

    mutate = () => {
        const city = clone(this.state.city);
        delete city._id;

        console.log(city)

        this.props.client.mutate({
            mutation: gql`mutation ($_id: ID, $input: City_Input) {
                city (
                    _id: $_id,
                    input: $input
                ) {
                    updatedAt
                }
            }`,
            variables: {
                _id: this.state.city._id,
                input: city
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Ciudad moficada correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const model = bindModel(this);
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div>
                    <Form fileds={this.createFieldObject(model)} onSubmit={this.mutate} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                        redirectUrl={'/admin/city'}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    graphql(city, {
        options: (props) => ({
            variables: {id: props.match.params.id === 'new' ? '' : props.match.params.id}
        })
    })
)(EditCity);
