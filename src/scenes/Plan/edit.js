import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Form from './../../components/Form';
import {get, setWith, clone} from 'lodash';
import AlertDialog from './../../components/Dialog';

const plan = gql`query ($id: String) {
    plan (_id: $id){
        _id
        name
        days,
        departures {
            label,
            dates {
                _id
                date
            }
        },
        cities {
            _id
        },
        countries {
            _id
        },
        prices {
            _id
            label
            amount,
            currency {
                _id
            }
        },
        itinerary {
            title
            description
        },
        include {
            text
        },
        notInclude {
            text
        }
    },
    currency {
        _id
        name
        code
        symbol
    },
    country {
        _id
        name
        isoCode
    },
    city {
        _id
        name
    }
}`;

function bindModel(context) {
    return function model(path) {
        const value = get(context.state, path, '');

        return {
            value,
            onChange(event) {
                const newValue = typeof event === 'object' ? event.target.value : event;
                const newState = setWith(clone(context.state), path, newValue, clone);
                context.setState(newState);
            }
        };
    };
}

class EditPlan extends Component {
    state = {
        plan: {},
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    componentWillReceiveProps (nextProps) {
        this.setState({city: nextProps.data.city});
        this.setState({country: nextProps.data.country});
        this.setState({currency: nextProps.data.currency});

        if(this.props.match.params.id === 'new') {
            this.setState({plan: {
                name: '',
                days: '',
                prices: [
                    { label: '', currency: { _id: '' }, amount: '' }
                ],
                countries: [
                    { _id: '' }
                ],
                cities: [
                    { _id: '' }
                ],
                departures: {
                    label: '',
                    dates: [
                        { date: '' }
                    ]
                },
                itinerary: [
                    { title: '', description: '' }
                ],
                include: [
                    { text: '' }
                ],
                notInclude: [
                    { text: '' }
                ]
            }});
        } else {
            this.setState({plan: nextProps.data.plan[0]});
        }
    };

    createFieldObject (model) {
        const prices = this.state.plan.prices.map((price, index) => {
            return [
                { label: 'Descripción', model: model('plan.prices['+index+'].label'), size: 6, type: 'text' },
                { label: 'Moneda', model: model('plan.prices['+index+'].currency._id'), size: 3, type: 'autoComplete', suggestions: this.state.currency },
                { label: 'Monto', model: model('plan.prices['+index+'].amount'), size: 3, type: 'currency' }
            ]
        });

        const countries = this.state.plan.countries.map((price, index) => {
            return [
                { label: 'País', model: model('plan.countries['+index+']._id'), size: 12, type: 'autoComplete', suggestions: this.state.country },
            ]
        });

        const cities = this.state.plan.cities.map((price, index) => {
            return [
                { label: 'Ciudad', model: model('plan.cities['+index+']._id'), size: 12, type: 'autoComplete', suggestions: this.state.city },
            ]
        });

        const dates = this.state.plan.departures.dates.map((price, index) => {
            return [
                { label: 'Fecha', model: model('plan.departures.dates['+index+'].date'), type:'date', size: 3 },
            ]
        });

        const itineraty = this.state.plan.itinerary.map((price, index) => {
            return [
                { label: 'Título', model: model('plan.itinerary['+index+'].title'), type:'text', size: 12 },
                { label: 'Descripción', model: model('plan.itinerary['+index+'].description'), type:'textarea', size: 12 }
            ]
        });

        const include = this.state.plan.include.map((price, index) => {
            return [
                { label: 'Item', model: model('plan.include['+index+'].text'), type:'text', size: 6 },
            ]
        });

        const notInclude = this.state.plan.notInclude.map((price, index) => {
            return [
                { label: 'Item', model: model('plan.notInclude['+index+'].text'), type:'text', size: 6 },
            ]
        });

        return [
            { label: 'Nombre', model: model('plan.name'), size: 8, type: 'text' },
            { label: 'Numero de dias', model: model('plan.days'), size: 4, type: 'number' },
            { label: 'Precios', type: 'multiField', fields: prices, addMore: this.addNewFieldsRow('plan.prices'), removeRow: this.removeFieldsRow('plan.prices') },
            { label: 'Países', type: 'multiField', fields: countries, addMore: this.addNewFieldsRow('plan.countries'), removeRow: this.removeFieldsRow('plan.countries') },
            { label: 'Ciudades', type: 'multiField', fields: cities, addMore: this.addNewFieldsRow('plan.cities'), removeRow: this.removeFieldsRow('plan.cities') },
            { label: 'Salidas', model: model('plan.departures.label'), size: 12, type: 'text' },
            { label: 'Fechas de Salidas', type: 'multiField', fields: dates, addMore: this.addNewFieldsRow('plan.departures.dates'), removeRow: this.removeFieldsRow('plan.departures.dates') },
            { label: 'Itinerario', type: 'multiField', fields: itineraty, addMore: this.addNewFieldsRow('plan.itinerary'), removeRow: this.removeFieldsRow('plan.itinerary') },
            { label: 'Incluye', type: 'multiField', fields: include, addMore: this.addNewFieldsRow('plan.include'), removeRow: this.removeFieldsRow('plan.include') },
            { label: 'No incluye', type: 'multiField', fields: notInclude, addMore: this.addNewFieldsRow('plan.notInclude'), removeRow: this.removeFieldsRow('plan.notInclude') },
        ]
    };

    addNewFieldsRow = (path) => {
        return () => {
            let newValue;
            if(path === 'plan.prices') {
                path = 'plan.prices['+this.state.plan.prices.length+']';
                newValue = { label: '', currency: { _id: '' }, amount: '' };
            } else if(path === 'plan.countries') {
                path = 'plan.countries['+this.state.plan.countries.length+']';
                newValue = { _id: '' };
            } else if(path === 'plan.cities') {
                path = 'plan.cities['+this.state.plan.cities.length+']';
                newValue = { _id: '' };
            } else if(path === 'plan.departures.dates') {
                path = 'plan.departures.dates['+this.state.plan.departures.dates.length+']';
                newValue = { date: '' };
            } else if(path === 'plan.itinerary') {
                path = 'plan.itinerary['+this.state.plan.itinerary.length+']';
                newValue = { title: '', description: '' };
            } else if(path === 'plan.include') {
                path = 'plan.include['+this.state.plan.include.length+']';
                newValue = { text: '' };
            } else if(path === 'plan.notInclude') {
                path = 'plan.notInclude['+this.state.plan.notInclude.length+']';
                newValue = { text: '' };
            }

            const newState = setWith(clone(this.state), path, newValue, clone);
            this.setState(newState);
        }
    };

    removeFieldsRow = (path) => {
        return (index) => {
            let newRow = clone(get(this.state, path, ''));
            newRow.splice(index, 1);

            if(newRow.length === 0) {
                if(path === 'plan.prices') {
                    newRow.push({ label: '', currency: { _id: '' }, amount: '' })
                } else if(path === 'plan.countries') {
                    newRow.push({ _id: '' })
                } else if(path === 'plan.cities') {
                    newRow.push({ _id: '' })
                } else if(path === 'plan.departures.dates') {
                    newRow.push({ date: '' })
                } else if(path === 'plan.itinerary') {
                    newRow.push({ title: '', description: '' })
                } else if(path === 'plan.include') {
                    newRow.push({ text: '' })
                } else if(path === 'plan.notInclude') {
                    newRow.push({ text: '' })
                }
            }

            const newState = setWith(clone(this.state), path, newRow, clone);
            this.setState(newState);
        }
    };

    mutatePlan = () => {
        const plan = clone(this.state.plan);
        delete plan._id;
        this.props.client.mutate({
            mutation: gql`mutation ($_id: ID, $input: Plan_Input) {
                plan (
                    _id: $_id,
                    input: $input
                ) {
                    updatedAt
                }
            }`,
            variables: {
                _id: this.state.plan._id,
                input: plan
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Plan moficado correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const model = bindModel(this);
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div>
                    {this.state.plan.prices && this.state.plan.countries &&
                    this.state.plan.cities && this.state.plan.departures &&
                    this.state.plan.itinerary && this.state.plan.include && this.state.plan.notInclude ? <Form fileds={this.createFieldObject(model)} onSubmit={this.mutatePlan} /> : ''}
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                        redirectUrl={'/admin'}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    graphql(plan, {
        options: (props) => ({
            variables: {id: props.match.params.id === 'new' ? '' : props.match.params.id}
        })
    })
)(EditPlan);
