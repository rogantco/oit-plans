import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Form from './../../components/Form';
import {get, setWith, clone} from 'lodash';
import AlertDialog from './../../components/Dialog';

const country = gql`query ($id: String) {
    country (_id: $id) {
        _id
        name
        isoCode
    }
}`;

function bindModel(context) {
    return function model(path) {
        const value = get(context.state, path, '');

        return {
            value,
            onChange(event) {
                const newValue = typeof event === 'object' ? event.target.value : event;
                const newState = setWith(clone(context.state), path, newValue, clone);
                context.setState(newState);
            }
        };
    };
}

class EditCountry extends Component {
    state = {
        country: {},
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    componentWillReceiveProps (nextProps) {
        if(this.props.match.params.id === 'new') {
            this.setState({country: {
                name: '',
                isoCode: ''
            }});
        } else {
            this.setState({country: nextProps.data.country[0]});
        }
    };

    createFieldObject (model) {
        return [
            { label: 'Nombre', model: model('country.name'), size: 12, type: 'text' },
            { label: 'Codigo ISO', model: model('country.isoCode'), size: 12, type: 'text' }
        ]
    };

    mutate = () => {
        const country = clone(this.state.country);
        delete country._id;
        this.props.client.mutate({
            mutation: gql`mutation ($_id: ID, $input: Country_Input) {
                country (
                    _id: $_id,
                    input: $input
                ) {
                    updatedAt
                }
            }`,
            variables: {
                _id: this.state.country._id,
                input: country
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'País moficado correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const model = bindModel(this);
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div>
                    <Form fileds={this.createFieldObject(model)} onSubmit={this.mutate} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                        redirectUrl={'/admin/country'}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    graphql(country, {
        options: (props) => ({
            variables: {id: props.match.params.id === 'new' ? '' : props.match.params.id}
        })
    })
)(EditCountry);
