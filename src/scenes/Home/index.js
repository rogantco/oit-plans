import React, { Component } from 'react';
import './styles.css';
import ListTable from './../../components/ListTable';
import { graphql, withApollo, compose } from 'react-apollo';
import gql from 'graphql-tag';
import Button from 'material-ui/Button';
import { withRouter } from 'react-router'
import AlertDialog from './../../components/Dialog';

const columnData = [
    { id: 'name', numeric: false, disablePadding: false, label: 'Nombre' },
    { id: 'days', numeric: false, disablePadding: false, label: 'Dias' },
    { id: 'createdAt', numeric: false, disablePadding: false, label: 'Creado' },
    { id: 'updatedAt', numeric: false, disablePadding: false, label: 'Modificado' }
];

const plansList = gql`query {
    plan {
        _id
        name
        days,
        createdAt,
        updatedAt
    }
}`;


class Home extends Component {
    state = {
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    createPlan = () => {
        this.props.history.push('/admin/plan/new');
    };

    deletePlan = (selected) => {
        this.props.client.mutate({
            mutation: gql`mutation ($_ids: [ID!]) {
                planDelete (_ids: $_ids) {
                    _id
                }
            }`,
            variables: {
                _ids: selected
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Plan(es) eliminado(s) correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div className="App">
                    <Button type='submit' raised color="primary" onClick={this.createPlan}>
                        Nuevo Plan
                    </Button>
                    <ListTable columns={columnData} rows={data.plan} title={'Planes'} url={'/admin/plan/'} handleDelete={this.deletePlan} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                    />
                </div>
            )
        }
    }
};

export default compose(
    withApollo,
    withRouter,
    graphql(plansList)
)(Home);
