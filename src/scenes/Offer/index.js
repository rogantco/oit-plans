import React, { Component } from 'react';
import ListTable from './../../components/ListTable';
import { graphql, withApollo, compose } from 'react-apollo';
import gql from 'graphql-tag';
import { withRouter } from 'react-router'
import AlertDialog from './../../components/Dialog';

const columnData = [
    { id: 'name', numeric: false, disablePadding: false, label: 'Nombre' },
    { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
    { id: 'city', numeric: false, disablePadding: false, label: 'Ciudad' },
    { id: 'phone', numeric: false, disablePadding: false, label: 'Telefono' },
    { id: 'plans', plans: true, disablePadding: false, label: 'Planes' },
    { id: 'createdAt', numeric: false, disablePadding: false, label: 'Creado' },
];

const offerList = gql`query {
    offer {
        _id
        name
        email
        city
        phone
        plans {
            _id,
            name
        }
        createdAt
    }
}`;


class Offer extends Component {
    state = {
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    render() {
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div className="App">
                    <ListTable columns={columnData} rows={data.offer} title={'Cotizaciones'} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    withRouter,
    graphql(offerList)
)(Offer);
