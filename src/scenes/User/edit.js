import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Form from './../../components/Form';
import {get, setWith, clone} from 'lodash';
import AlertDialog from './../../components/Dialog';

const user = gql`query ($id: String) {
    user (_id: $id) {
        _id
        name
        email
        role
    }
}`;

function bindModel(context) {
    return function model(path) {
        const value = get(context.state, path, '');

        return {
            value,
            onChange(event) {
                const newValue = typeof event === 'object' ? event.target.value : event;
                const newState = setWith(clone(context.state), path, newValue, clone);
                context.setState(newState);
            }
        };
    };
}

class EditUser extends Component {
    state = {
        user: {},
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    componentWillReceiveProps (nextProps) {
        if(this.props.match.params.id === 'new') {
            this.setState({user: {
                    name: '',
                    email: '',
                    //role: ''
                }});
        } else {
            this.setState({user: nextProps.data.user[0]});
        }
    };

    createFieldObject (model) {
        return [
            { label: 'Nombre', model: model('user.name'), size: 12, type: 'text' },
            { label: 'Email', model: model('user.email'), size: 12, type: 'text' },
            { label: 'Rol', model: model('user.role'), size: 12, type: 'text' },
            { label: 'Contraseña', model: model('user.password'), size: 12, type: 'password' }
        ]
    };

    mutate = () => {
        const user = clone(this.state.user);
        delete user._id;
        this.props.client.mutate({
            mutation: gql`mutation ($_id: ID, $input: User_Input) {
                user (
                    _id: $_id,
                    input: $input
                ) {
                    updatedAt
                }
            }`,
            variables: {
                _id: this.state.user._id,
                input: user
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Usuario moficado correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const model = bindModel(this);
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div>
                    <Form fileds={this.createFieldObject(model)} onSubmit={this.mutate} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                        redirectUrl={'/admin/user'}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    graphql(user, {
        options: (props) => ({
            variables: {id: props.match.params.id === 'new' ? '' : props.match.params.id}
        })
    })
)(EditUser);
