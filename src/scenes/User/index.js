import React, { Component } from 'react';
import ListTable from './../../components/ListTable';
import { graphql, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Button from 'material-ui/Button';
import { withRouter } from 'react-router'
import { compose } from 'react-apollo';
import AlertDialog from './../../components/Dialog';

const columnData = [
    { id: 'name', numeric: false, disablePadding: false, label: 'Nombre' },
    { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
    { id: 'role', numeric: false, disablePadding: false, label: 'Rol' },
    { id: 'createdAt', numeric: false, disablePadding: false, label: 'Creado' },
    { id: 'updatedAt', numeric: false, disablePadding: false, label: 'Modificado' }
];

const usersList = gql`query {
    user {
        _id
        name
        email
        role
        createdAt
        updatedAt
    }
}`;


class User extends Component {
    state = {
        dialog: {
            open: false,
            error: false,
            message: ''
        }
    };

    createUser = () => {
        this.props.history.push('/admin/user/new');
    };

    deleteUser = (selected) => {
        this.props.client.mutate({
            mutation: gql`mutation ($_ids: [ID!]) {
                userDelete (_ids: $_ids) {
                    _id
                }
            }`,
            variables: {
                _ids: selected
            }
        }).then(() => {
            this.setState({ dialog: {open: true, message: 'Usuario(s) eliminado(s) correctamente'} });
        }).catch((err) => {
            this.setState({ dialog: {open: true, error: true, message: JSON.stringify(err)}});
        });
    };

    render() {
        const {data} = this.props;

        if(data.loading) {
            return(
                <p>Loading...</p>
            )
        } else {
            return(
                <div className="App">
                    <Button type='submit' raised color="primary" onClick={this.createUser}>
                        Nuevo Usuario
                    </Button>
                    <ListTable columns={columnData} rows={data.user} title={'Usuarios'} url={'/admin/user/'} handleDelete={this.deleteUser} />
                    <AlertDialog
                        open={this.state.dialog.open}
                        error={this.state.dialog.error}
                        message={this.state.dialog.message}
                    />
                </div>
            )
        }
    }
}

export default compose(
    withApollo,
    withRouter,
    graphql(usersList)
)(User);
