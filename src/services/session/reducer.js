const localStorage = window.localStorage;
const session = JSON.parse(localStorage.getItem('session'));

const initialState = session ? session : {
    _id: '',
    email: '',
    name: '',
    token: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOGGING_USER':
            return {
                ...state,
                _id: action.payload._id,
                email: action.payload.email,
                name: action.payload.name,
                token: action.payload.token
            };

        default:
            return state;
    }
};
