import { push } from 'react-router-redux';

export const loggingUser = (payload) => {
    return (dispatch) => {
        dispatch({
            type: 'LOGGING_USER',
            payload: payload
        });

        const localStorage = window.localStorage;
        localStorage.setItem('session', JSON.stringify(payload));
        dispatch(push('/admin'));
    }
};
